import gc
import MNIST_handler
from sys import argv

print("Unzipping raw data")
MNIST_handler.unzip_raw()
gc.collect()

argv = [x.lower().strip() for x in argv]
if 'all' in argv:
    MNIST_handler.CSV.make_all_from_raw()
    gc.collect()
    MNIST_handler.JSON.make_all_from_raw()
    gc.collect()
    MNIST_handler.LISTS.make_all_from_raw()
    gc.collect()
    MNIST_handler.NUMPY.make_all_from_raw()
    gc.collect()
else:
    if 'csv' in argv:
        MNIST_handler.CSV.make_all_from_raw()
        gc.collect()
    if 'json' in argv:
        MNIST_handler.JSON.make_all_from_raw()
        gc.collect()
    if 'list' in argv or 'lists' in argv or 'pickled_list' in argv or 'pickled_lists' in argv:
        MNIST_handler.LISTS.make_all_from_raw()
        gc.collect()
    if 'numpy' in argv or 'nparr' in argv or 'numpy_array' in argv or 'pickled_numpy' in argv or 'pickled_numpy_array' in argv:
        MNIST_handler.NUMPY.make_all_from_raw()
        gc.collect()
