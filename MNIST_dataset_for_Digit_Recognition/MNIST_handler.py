'''
Original data source: http://yann.lecun.com/exdb/mnist/
NOTE: the extracted data files from the above URL have been renamed for clarity, as follows:
    train-images-idx3-ubyte    --->    (training set images) train-images-idx3-ubyte
    train-labels-idx1-ubyte    --->    (training set labels) train-labels-idx1-ubyte
    t10k-images-idx3-ubyte     --->    (test set images) t10k-images-idx3-ubyte
    t10k-labels-idx1-ubyte     --->    (test set labels) t10k-labels-idx1-ubyte
'''

from zipfile import ZipFile
import csv
import pickle
import json
import os
from inspect import getsourcefile
from os.path import abspath
import numpy as np
import gc
import sys

def get_directory_path_of_current_file():
    ## Source: http://stackoverflow.com/a/18489147/4900327
    current_file_path = abspath(getsourcefile(lambda: 0))
    current_file_path.split("/")
    return '/'.join(current_file_path.split("/")[:-1])+'/'

CURRENT_FILE_DIR = get_directory_path_of_current_file()


RAW_TRAINING_INPUTS_FILE = CURRENT_FILE_DIR+'Raw/(training set images) train-images-idx3-ubyte'
RAW_TRAINING_TARGETS_FILE = CURRENT_FILE_DIR+'Raw/(training set labels) train-labels-idx1-ubyte'
TRAINING_SET_SIZE = 60000

RAW_TEST_INPUTS_FILE = CURRENT_FILE_DIR+'Raw/(test set images) t10k-images-idx3-ubyte'
RAW_TEST_TARGETS_FILE = CURRENT_FILE_DIR+'Raw/(test set labels) t10k-labels-idx1-ubyte'
TEST_SET_SIZE = 10000

NUM_FEATURES = 28*28

def unzip_raw():
    '''Extract raw files into directory ./Raw/'''
    ## Source for `zipfile`: https://stackoverflow.com/a/36662770/4900327
    with ZipFile('MNIST dataset (renamed).zip', 'r') as zip_inp:
        zip_inp.extractall('Raw')


'''From files in ./Raw/, return Python lists.'''
def convert_raw_to_list(inputs_filename, targets_filename, num_examples):
    inputs_f = open(inputs_filename, 'rb')  ## File pointer to inputs of training/test set
    inputs_f.read(16)
    targets_f = open(targets_filename, 'rb')    ## File pointer to targets of training/test set
    targets_f.read(8)

    inputs = []
    targets = []
    for i in range(0, num_examples):
        inputs.append( [ord(inputs_f.read(1)) for j in range(0, 28*28)] )
        targets.append(ord(targets_f.read(1)))

    inputs_f.close()
    targets_f.close()
    return inputs, targets


def get_MNIST_training_set():
    return convert_raw_to_list(RAW_TRAINING_INPUTS_FILE, RAW_TRAINING_TARGETS_FILE, TRAINING_SET_SIZE)


def get_MNIST_test_set():
    return convert_raw_to_list(RAW_TEST_INPUTS_FILE, RAW_TEST_TARGETS_FILE, TEST_SET_SIZE)




class CSV:
    @classmethod
    def make_all_from_raw(cls):
        '''From files in ./Raw/, save CSV files in directory ./CSVs/'''
        dir_name = CURRENT_FILE_DIR+'CSV'
        CSV_TRAINING_SET_FILE = 'MNIST_training_set.csv'
        CSV_TEST_SET_FILE = 'MNIST_test_set.csv'

        ## https://pjreddie.com/projects/mnist-in-csv/
        def convert_and_save_raw_to_csv(imgf, labelf, outf, n):
            f = open(imgf, "rb")
            o = open(outf, "w")
            l = open(labelf, "rb")

            f.read(16)
            l.read(8)
            images = []

            for i in range(n):
                image = [ord(l.read(1))]
                for j in range(28*28):
                    image.append(ord(f.read(1)))
                images.append(image)

            for image in images:
                o.write(",".join(str(pix) for pix in image)+"\n")
            f.close()
            o.close()
            l.close()

        print("\nMaking CSV format...", end='')

        ## Make directory ./CSVs/
        if not os.path.exists(dir_name):
            os.makedirs(dir_name)

        ## Convert training set:
        convert_and_save_raw_to_csv(
            RAW_TRAINING_INPUTS_FILE,
            RAW_TRAINING_TARGETS_FILE,
            dir_name+"/"+CSV_TRAINING_SET_FILE, TRAINING_SET_SIZE)

        ## Convert test set:
        convert_and_save_raw_to_csv(
            RAW_TEST_INPUTS_FILE,
            RAW_TEST_TARGETS_FILE,
            dir_name+"/"+CSV_TEST_SET_FILE, TEST_SET_SIZE)

        print('done.')



class JSON():
    '''From files in ./Raw/, create json and save in directory ./JSON/'''
    dir_name = CURRENT_FILE_DIR+'JSON'
    TRAINING_INPUTS_FILE = 'MNIST_training_inputs.json'
    TRAINING_TARGETS_FILE = 'MNIST_training_targets.json'
    TEST_INPUTS_FILE = 'MNIST_test_inputs.json'
    TEST_TARGETS_FILE = 'MNIST_test_targets.json'

    @classmethod
    def make_training_set_from_raw(cls):
        ## Make directory:
        if not os.path.exists(cls.dir_name):
            os.makedirs(cls.dir_name)
        ## Load from raw into lists:
        training_inputs, training_targets = get_MNIST_training_set()
        ## Save to JSON:
        with open(cls.dir_name+"/"+cls.TRAINING_INPUTS_FILE, 'w') as json_out:
            json_out.write(json.dumps(training_inputs, separators=(',', ':')))
        with open(cls.dir_name+"/"+cls.TRAINING_TARGETS_FILE, 'w') as json_out:
            json_out.write(json.dumps(training_targets, separators=(',', ':')))

    @classmethod
    def make_test_set_from_raw(cls):
        ## Make directory:
        if not os.path.exists(cls.dir_name):
            os.makedirs(cls.dir_name)
        ## Load from raw into lists:
        test_inputs, test_targets = get_MNIST_test_set()
        ## Save to JSON:
        with open(cls.dir_name+"/"+cls.TEST_INPUTS_FILE, 'w') as json_out:
            json_out.write(json.dumps(test_inputs, separators=(',', ':')))
        with open(cls.dir_name+"/"+cls.TEST_TARGETS_FILE, 'w') as json_out:
            json_out.write(json.dumps(test_targets, separators=(',', ':')))

    @classmethod
    def make_all_from_raw(cls):
        print("\nMaking JSON format...", end='')
        cls.make_training_set_from_raw()
        cls.make_test_set_from_raw()
        print('done.')
        cls.check_loading_from_disk()



    @classmethod
    def load_training_inputs(cls):
        training_inputs = None
        with open(cls.dir_name+"/"+cls.TRAINING_INPUTS_FILE, 'r') as json_inp:
            training_inputs = json.loads(json_inp.read())
        return training_inputs

    @classmethod
    def load_training_targets(cls):
        training_targets = None
        with open(cls.dir_name+"/"+cls.TRAINING_TARGETS_FILE, 'r') as json_inp:
            training_targets = json.loads(json_inp.read())
        return training_targets

    @classmethod
    def load_test_inputs(cls):
        test_inputs = None
        with open(cls.dir_name+"/"+cls.TEST_INPUTS_FILE, 'r') as json_inp:
            test_inputs = json.loads(json_inp.read())
        return test_inputs

    @classmethod
    def load_test_targets(cls):
        test_targets = None
        with open(cls.dir_name+"/"+cls.TEST_TARGETS_FILE, 'r') as json_inp:
            test_targets = json.loads(json_inp.read())
        return test_targets

    @classmethod
    def load_all(cls):
        return cls.load_training_inputs(), cls.load_training_targets(), cls.load_test_inputs(), cls.load_test_targets()

    @classmethod
    def check_loading_from_disk(cls):
        print("Testing loading from disk...", end='')
        training_inputs, training_targets, test_inputs, test_targets = cls.load_all()
        assert len(training_inputs) == TRAINING_SET_SIZE
        for eg in training_inputs:
            assert(len(eg)) == NUM_FEATURES
        assert len(training_targets) == TRAINING_SET_SIZE

        assert len(test_inputs) == TEST_SET_SIZE
        for eg in test_inputs:
            assert(len(eg)) == NUM_FEATURES
        assert len(test_targets) == TEST_SET_SIZE
        print("success.")



class LISTS():
    '''From files in ./Raw/, save Python lists in directory ./LISTS/'''
    dir_name = CURRENT_FILE_DIR+'LISTS'
    TRAINING_INPUTS_FILE = 'MNIST_training_inputs.pickle'
    TRAINING_TARGETS_FILE = 'MNIST_training_targets.pickle'
    TEST_INPUTS_FILE = 'MNIST_test_inputs.pickle'
    TEST_TARGETS_FILE = 'MNIST_test_targets.pickle'

    @classmethod
    def make_training_set_from_raw(cls):
        ## Make directory:
        if not os.path.exists(cls.dir_name):
            os.makedirs(cls.dir_name)
        ## Load from raw into lists:
        training_inputs, training_targets = get_MNIST_training_set()
        ## Save to pickle:
        with open(cls.dir_name+"/"+cls.TRAINING_INPUTS_FILE, 'wb') as pickle_out:
            pickle.dump(training_inputs, pickle_out, protocol=pickle.HIGHEST_PROTOCOL)
        with open(cls.dir_name+"/"+cls.TRAINING_TARGETS_FILE, 'wb') as pickle_out:
            pickle.dump(training_targets, pickle_out, protocol=pickle.HIGHEST_PROTOCOL)

    @classmethod
    def make_test_set_from_raw(cls):
        ## Make directory:
        if not os.path.exists(cls.dir_name):
            os.makedirs(cls.dir_name)
        ## Load from raw into lists:
        test_inputs, test_targets = get_MNIST_test_set()
        ## Save to pickle:
        with open(cls.dir_name+"/"+cls.TEST_INPUTS_FILE, 'wb') as pickle_out:
            pickle.dump(test_inputs, pickle_out, protocol=pickle.HIGHEST_PROTOCOL)
        with open(cls.dir_name+"/"+cls.TEST_TARGETS_FILE, 'wb') as pickle_out:
            pickle.dump(test_targets, pickle_out, protocol=pickle.HIGHEST_PROTOCOL)

    @classmethod
    def make_all_from_raw(cls):
        print("\nMaking LISTS format...", end='')
        cls.make_training_set_from_raw()
        cls.make_test_set_from_raw()
        print('done.')
        cls.check_loading_from_disk()



    @classmethod
    def load_training_inputs(cls):
        training_inputs = None
        with open(cls.dir_name+"/"+cls.TRAINING_INPUTS_FILE, 'rb') as pickle_inp:
            training_inputs = pickle.load(pickle_inp)
        return training_inputs

    @classmethod
    def load_training_targets(cls):
        training_targets = None
        with open(cls.dir_name+"/"+cls.TRAINING_TARGETS_FILE, 'rb') as pickle_inp:
            training_targets = pickle.load(pickle_inp)
        return training_targets

    @classmethod
    def load_test_inputs(cls):
        test_inputs = None
        with open(cls.dir_name+"/"+cls.TEST_INPUTS_FILE, 'rb') as pickle_inp:
            test_inputs = pickle.load(pickle_inp)
        return test_inputs

    @classmethod
    def load_test_targets(cls):
        test_targets = None
        with open(cls.dir_name+"/"+cls.TEST_TARGETS_FILE, 'rb') as pickle_inp:
            test_targets = pickle.load(pickle_inp)
        return test_targets

    @classmethod
    def load_all(cls):
        return cls.load_training_inputs(), cls.load_training_targets(), cls.load_test_inputs(), cls.load_test_targets()

    @classmethod
    def check_loading_from_disk(cls):
        print("Testing loading from disk...", end='')
        training_inputs, training_targets, test_inputs, test_targets = cls.load_all()
        assert len(training_inputs) == TRAINING_SET_SIZE
        for eg in training_inputs:
            assert(len(eg)) == NUM_FEATURES
        assert len(training_targets) == TRAINING_SET_SIZE

        assert len(test_inputs) == TEST_SET_SIZE
        for eg in test_inputs:
            assert(len(eg)) == NUM_FEATURES
        assert len(test_targets) == TEST_SET_SIZE
        print("success.")





class NUMPY():
    '''From files in ./Raw/, create Numpy Arrays and save in directory ./NUMPY/'''
    dir_name = CURRENT_FILE_DIR+'NUMPY'
    TRAINING_INPUTS_FILE = 'MNIST_training_inputs_numpy.pickle'
    TRAINING_TARGETS_FILE = 'MNIST_training_targets_numpy.pickle'
    TEST_INPUTS_FILE = 'MNIST_test_inputs_numpy.pickle'
    TEST_TARGETS_FILE = 'MNIST_test_targets_numpy.pickle'

    @classmethod
    def make_training_set_from_raw(cls):
        ## Make directory:
        if not os.path.exists(cls.dir_name):
            os.makedirs(cls.dir_name)
        ## Load from raw into lists:
        training_inputs, training_targets = get_MNIST_training_set()
        ## Convert to a numpy array:
        training_inputs = np.array(training_inputs, dtype='uint8')
        training_targets = np.array(training_targets, dtype='uint8')
        ## Save to pickle:
        with open(cls.dir_name+"/"+cls.TRAINING_INPUTS_FILE, 'wb') as pickle_out:
            pickle.dump(training_inputs, pickle_out, protocol=pickle.HIGHEST_PROTOCOL)
        with open(cls.dir_name+"/"+cls.TRAINING_TARGETS_FILE, 'wb') as pickle_out:
            pickle.dump(training_targets, pickle_out, protocol=pickle.HIGHEST_PROTOCOL)

    @classmethod
    def make_test_set_from_raw(cls):
        ## Make directory:
        if not os.path.exists(cls.dir_name):
            os.makedirs(cls.dir_name)
        ## Load from raw into lists:
        test_inputs, test_targets = get_MNIST_test_set()
        ## Convert to a numpy array:
        test_inputs = np.array(test_inputs, dtype='uint8')
        test_targets = np.array(test_targets, dtype='uint8')
        ## Save to pickle:
        with open(cls.dir_name+"/"+cls.TEST_INPUTS_FILE, 'wb') as pickle_out:
            pickle.dump(test_inputs, pickle_out, protocol=pickle.HIGHEST_PROTOCOL)
        with open(cls.dir_name+"/"+cls.TEST_TARGETS_FILE, 'wb') as pickle_out:
            pickle.dump(test_targets, pickle_out, protocol=pickle.HIGHEST_PROTOCOL)

    @classmethod
    def make_all_from_raw(cls):
        print("\nMaking NUMPY format...", end='')
        cls.make_training_set_from_raw()
        cls.make_test_set_from_raw()
        print('done.')
        cls.check_loading_from_disk()



    @classmethod
    def load_training_inputs(cls):
        training_inputs = None
        with open(cls.dir_name+"/"+cls.TRAINING_INPUTS_FILE, 'rb') as pickle_inp:
            training_inputs = pickle.load(pickle_inp)
        return training_inputs

    @classmethod
    def load_training_targets(cls):
        training_targets = None
        with open(cls.dir_name+"/"+cls.TRAINING_TARGETS_FILE, 'rb') as pickle_inp:
            training_targets = pickle.load(pickle_inp)
        return training_targets

    @classmethod
    def load_test_inputs(cls):
        test_inputs = None
        with open(cls.dir_name+"/"+cls.TEST_INPUTS_FILE, 'rb') as pickle_inp:
            test_inputs = pickle.load(pickle_inp)
        return test_inputs

    @classmethod
    def load_test_targets(cls):
        test_targets = None
        with open(cls.dir_name+"/"+cls.TEST_TARGETS_FILE, 'rb') as pickle_inp:
            test_targets = pickle.load(pickle_inp)
        return test_targets

    @classmethod
    def load_all(cls):
        return cls.load_training_inputs(), cls.load_training_targets(), cls.load_test_inputs(), cls.load_test_targets()

    @classmethod
    def check_loading_from_disk(cls):
        print("Testing loading from disk...", end='')
        training_inputs, training_targets, test_inputs, test_targets = cls.load_all()
        assert len(training_inputs) == TRAINING_SET_SIZE
        for eg in training_inputs:
            assert(len(eg)) == NUM_FEATURES
        assert len(training_targets) == TRAINING_SET_SIZE

        assert len(test_inputs) == TEST_SET_SIZE
        for eg in test_inputs:
            assert(len(eg)) == NUM_FEATURES
        assert len(test_targets) == TEST_SET_SIZE
        print("success.")
        print("NUMPY datatype: %s"%type(training_inputs))



















def make_mnist_CSVs():
    ## Before we make the CSV files (which takes time), we check if the files already
    ## exist in the current directory, and if they are each greater than 1 MB in size
    ## (ideally, `mnist_train.csv` is more than 100 MB and `mnist_test.csv` is more than 15 MB)
    ## If either of these conditions fails, we remake the files.
    current_dir_path = get_directory_path_of_current_file()
    current_directory = os.listdir(current_dir_path)

    mnist_train_csv_file = 'mnist_train.csv'

    if mnist_train_csv_file not in current_directory or os.stat(current_dir_path + mnist_train_csv_file).st_size < 1E6:  ## Source for os.stat(...) => http://stackoverflow.com/a/10960485/4900327
        convert(current_dir_path + "MNIST dataset (renamed)/(training set images) train-images-idx3-ubyte",
                current_dir_path + "MNIST dataset (renamed)/(training set labels) train-labels-idx1-ubyte",
                current_dir_path + mnist_train_csv_file, 60000)


    mnist_test_csv_file = 'mnist_test.csv'

    if mnist_test_csv_file not in current_directory or os.stat(current_dir_path + mnist_test_csv_file).st_size < 1E6: ## Source for os.stat(...) => http://stackoverflow.com/a/10960485/4900327
        convert(current_dir_path + "MNIST dataset (renamed)/(test set images) t10k-images-idx3-ubyte",
                current_dir_path + "MNIST dataset (renamed)/(test set labels) t10k-labels-idx1-ubyte",
                current_dir_path + mnist_test_csv_file, 10000)


def make_mnist_pickle():
    ## Before we make the pickle (which takes time), we check if the file already
    ## exists in the current directory, and its size is greater than 1 MB.
    ## (ideally, `MNIST training and test set.pickle` is more than 100 MB.
    ## If either of these conditions fails, we remake the file.
    current_dir_path = get_directory_path_of_current_file()
    current_directory = os.listdir(current_dir_path)

    mnist_pickle_file = 'MNIST training and test set.pickle'
    mnist_test_csv_file = 'mnist_test.csv'
    mnist_train_csv_file = 'mnist_train.csv'

    if mnist_pickle_file not in current_directory or os.stat(current_dir_path + mnist_pickle_file).st_size < 1E6: ## Source for os.stat(...) => http://stackoverflow.com/a/10960485/4900327
        with open(current_dir_path + mnist_train_csv_file, 'r') as inp:
            train_stream = csv.reader(inp)
            train = [row for row in train_stream]

        train_inputs = []
        train_outputs = []
        for eg in train:
            train_inputs.append([int(x) for x in eg[1:]])
            train_outputs.append(int(eg[0]))


        with open(current_dir_path + mnist_test_csv_file, 'r') as inp:
            test_stream = csv.reader(inp)
            test = [row for row in test_stream]

        test_inputs = []
        test_outputs = []
        for eg in test:
            test_inputs.append([int(x) for x in eg[1:]])
            test_outputs.append(int(eg[0]))

        dataset = {'train_inputs':train_inputs, 'train_outputs':train_outputs, 'test_inputs':test_inputs, 'test_outputs':test_outputs}

        with open(current_dir_path + mnist_pickle_file, 'wb') as out:
            pickle.dump(dataset, out, protocol=pickle.HIGHEST_PROTOCOL)


def load_from_mnist_pickle():
    current_dir_path = get_directory_path_of_current_file()
    current_directory = os.listdir(current_dir_path)

    mnist_pickle_file = 'MNIST training and test set.pickle'

    if mnist_pickle_file not in current_directory or os.stat(current_dir_path + mnist_pickle_file).st_size < 1E6:  ## Source for os.stat(...) => http://stackoverflow.com/a/10960485/4900327
        make_mnist_CSVs()
        make_mnist_pickle()
    gc.collect()    ## Source: http://stackoverflow.com/a/1316793/4900327

    with open(current_dir_path + mnist_pickle_file, 'rb') as inp:
        dataset = pickle.load(inp)
    return dataset['train_inputs'], dataset['train_outputs'], dataset['test_inputs'], dataset['test_outputs']
