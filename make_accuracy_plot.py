import re
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns
with open('nn_most_recent.log', 'r') as inp:
  txt = inp.read()

accs = [float(a) for a in re.findall(pattern='Current accuracy: (.*?)% \t', string=txt)]
fig = plt.subplot(1, 1,1)
# Plot all accuracies.
fig.plot([i for i in range(0,len(accs))], accs)
fig.set_ylabel('Accuracy')
fig.set_xlabel('Batch No.')
fig.set_title('Network started at: %s'%(re.findall(pattern='Batch no. 1;(.*)Timestamp: (.*)', string=txt)[0][1]))

# Make red dots on the top, indicating the best accuracy points (and thus when the NN was saved to disk).
best_points_accs = []
best_points_batch_num = []
best_point_acc = -1
for i in range(0, len(accs)):
    if accs[i] > best_point_acc:
        best_point_acc = accs[i]
        best_points_accs.append(accs[i])
        best_points_batch_num.append(i)
fig.scatter(best_points_batch_num, best_points_accs, color='r', s=30)

plt.savefig('accuracy_plot.png', dpi=300)

