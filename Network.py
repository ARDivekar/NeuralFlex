# exp5.py: Backpropogration algorithm.

import numpy as np
from numpy import array as np_arr
import gc
from matplotlib import pyplot as plt
plt.ion()
from copy import deepcopy
import random
import time
import io
from datetime import datetime
import klepto
import os
import sys


class WeightInitialization:
    weight_initialization = ""
    weight_init_params = {}

    def __init__(self, weight_initialization, weight_init_params):
        self.weight_initialization = weight_initialization
        self.weight_init_params = weight_init_params

    def initialize(self, num_features, num_outputs, num_nodes_in_hidden_layers):
        if self.weight_initialization.lower() == 'standard':
            return self._standard_weight_initialization(num_features, num_outputs, num_nodes_in_hidden_layers)
        if self.weight_initialization.lower() == 'restore_klepto':
            return self._restore_klepto(1+len(num_nodes_in_hidden_layers) )



    def _standard_weight_initialization(self, num_features, num_outputs, num_nodes_in_hidden_layers):
        '''Generate a dictionary of several NxM numpy matrices of small random values.
        Each corresponds to the weight matrix of a hidden layer, or the output layer.'''
        neg_val = 0
        if self.weight_init_params.get('negative_weights') == True:
            neg_val = 0.5

        div_by = self.weight_init_params.get('div_by')
        if div_by == None:
            raise Exception(
                "`div_by` in dictionary `weight_init_params` must be given some numeric value, e.g. 1.0")

        if type(num_nodes_in_hidden_layers) != type([0]) or len(num_nodes_in_hidden_layers) == 0:
            return {1: (np.random.rand(num_features + 1, num_outputs) - neg_val) / div_by}

        ## Random initialize all required weight matrices by subtracting `neg_val` and dividing by `div_by`:
        ## Note: the Numpy array subtraction and division is 'rolled out' to prevent making copies of the
        ## matrices. See: https://stackoverflow.com/a/4319312/4900327
        init_weights = {}
        init_weights[1] = np.random.rand(num_features + 1, num_nodes_in_hidden_layers[0])
        init_weights[1] -= neg_val
        init_weights[1] /= div_by
        gc.collect()

        for i in range(1, len(num_nodes_in_hidden_layers)):
            init_weights[i + 1] = np.random.rand(num_nodes_in_hidden_layers[i - 1] + 1, num_nodes_in_hidden_layers[i])
            init_weights[i + 1] -= neg_val
            init_weights[i + 1] /= div_by
            gc.collect()

        init_weights[len(num_nodes_in_hidden_layers) + 1] = np.random.rand(num_nodes_in_hidden_layers[-1] + 1, num_outputs)
        init_weights[len(num_nodes_in_hidden_layers) + 1] -= neg_val
        init_weights[len(num_nodes_in_hidden_layers) + 1] /= div_by
        gc.collect()

        return init_weights



    def _restore_klepto(self, num_matrices):
        dir_archive_name = self.weight_init_params.get('dir_archive_name')
        if dir_archive_name == None:
            dir_archive_name = 'best_nn_model'
        inp_dir = klepto.archives.dir_archive(dir_archive_name, cached=True, serialized=True)
        weights = {}
        for i in range(1, num_matrices+1):
            inp_dir.load(i)
            weights[i] = inp_dir[i]
            inp_dir.clear()
            gc.collect()
        del inp_dir
        print("Restored weights from klepto archive `%s`. Matrix sizes: %s"%(dir_archive_name, [weights[i].shape for i in weights]))
        return weights


class Activation:
    activation_function = ""

    def __init__(self, activation_function):
        self.activation_function = activation_function

    def activate(self, net_matrix):
        '''
        :param net_matrix: a NumPy matrix
        :return: NumPy matrix of the `activated` values output from one Neural Network layer.
        '''
        if self.activation_function.lower() == 'sigmoid':
            return Activation.sigmoid(net_matrix)
        elif self.activation_function.lower() == 'relu':
            return Activation.ReLU(net_matrix)
        elif self.activation_function.lower() == "leaky relu" or self.activation_function.lower() == "leaky_relu":
            return Activation.Leaky_ReLU(net_matrix)

    @staticmethod
    def sigmoid(z):
        # a unipolar binary sigmoid, i.e. returns values between 0 and 1.
        return 1 / (1 + np.exp(-z))

    @staticmethod
    def ReLU(z):
        return np.maximum(z, 0)

    @staticmethod
    def Leaky_ReLU(z):
        # Source: stackoverflow.com/a/40013151/4900327
        return np.maximum(z, 0.01 * z)


class LocalGradient:
    '''Calculates the local gradient values (i.e. the derivative of the activation function, into which
    we pass the excitation i.e. "net" calculated by each neuron in the current layer),
    at every layer except the output.'''
    activation_function = ""

    def __init__(self, activation_function):
        self.activation_function = activation_function

    def calculate_local_gradient(self, net):
        '''
        Let:
            N_c = number_of_neurons_in_current_layer
            N_(c+1) = number_of_neurons_in_next_layer
            M_b = num_examples

        :param net: a (M_b)x(N_c) matrix of excitations from each of the N_c neurons of the current layer.
        :return: F'(net), a (M_b)x(N_c) matrix of local gradients of the N_c neurons of the current layer.
        '''

        if self.activation_function.lower() == "sigmoid":
            return self._sigmoid_gradient(net)
        elif self.activation_function.lower() == "relu":
            return self._ReLU_gradient(net)
        elif self.activation_function.lower() == "leaky relu" or self.activation_function.lower() == "leaky_relu":
            return self._Leaky_ReLU_gradient(net)

    def _sigmoid_gradient(self, net):
        # Remember, each column of `weights` is a neuron in the current layer.
        # Remember, each row of `augmented_inputs` is a training example passed
        # to the current layer from the previous layer.

        # net is a scalar, denoting sum(sum(w_i*x_i), over the entire batch.
        # In mini-batch gradient descent which ensures that the gradients
        # depend on the entire batch of inputs. That is encoded here.
        sig = Activation.sigmoid(net)
        return sig * (1 - sig)

    def _ReLU_gradient(self, net):
        net = deepcopy(net)
        net[net > 0] = 1
        net[net <= 0] = 0
        return net

    def _Leaky_ReLU_gradient(self, net):
        net = deepcopy(net)
        net[net > 0] = 1
        net[net <= 0] = 0.01  # Source: http://stackoverflow.com/a/10335159/4900327
        return net


class Encoding:
    problem_type = ""
    problem_params = {}

    def __init__(self, problem_type, problem_params):
        self.problem_type = problem_type
        self.problem_params = problem_params

    def encode_final_output(self, outputs):
        # Modify the output depending on whether we are performing
        # classification or regression.

        if self.problem_type.lower() == 'classification':
            return self._classification_encode(outputs)

        # Performing regression.
        elif self.problem_type.lower() == 'regression':
            return outputs

    def _classification_encode(self, outputs):
        # Doing classification, check if we allow the predicted output to have more than one `1` value.
        # local_representation == True, means that only one output is 1, all else are zero.
        # Otherwise we are said to have "distributed representation".

        local_representation = self.problem_params.get('local_representation')
        if local_representation == None:
            raise ValueError(
                'local_representation must be passed either a True or False value.')

        if local_representation == False:  # More than one output could be 1.
            # We predict the output is 1 if it is above the classification
            # threshold.
            classification_threshold = self.problem_params.get(
                'classification_threshold')
            if classification_threshold == None:
                raise ValueError(
                    'classification_threshold must be passed some numerical value.')
            # outputs = deepcopy(outputs)
            outputs[outputs > classification_threshold] = 1
            outputs[outputs <= classification_threshold] = 0

        else:  # Choose the max output of each row as 1.
            # If two indices have the same max value, choose the first one.
            # A column of indices of the max element in each row.
            row_max_indices = outputs.argmax(axis=1)
            # Make a matrix of zeros, with the same dimensions
            outputs = np.zeros_like(outputs)
            # Source: stackoverflow.com/a/20295159/4900327
            outputs[range(0, len(outputs)), row_max_indices] = 1

        return outputs

    @staticmethod
    def convert_outputs_integers_to_binary_vectors(dataset_targets, num_classes=-1):
        '''
        :param dataset_targets: an (M_b x 1) Numpy array which we have to convert to M_b binary vectors.
        :return: an (M_b x K) matrix of binary vectors.
        '''
        detected_classes = np.unique(np_arr(dataset_targets))

        # In general, assume that there may be classes which are not present.
        # e.g. if the targets are handwritten numbers, but the dataset does not include an example of '5',
        # we still make the binary vector from 0 to 9, e.g. [0,0,0,0,0,0,1,0,0,0] having 10 values.
        # Then, the output array of binary vectors will not have any example where the
        # bit corresponding to '5' is set to 1.
        # However, if the handwritten-number dataset is missing '0', '1' and '6', we will have
        # only the vector from the min to the max actually present. Thus, we will have
        # a binary vector from '2' to '9', e.g. [0,1,0,0,0,0,0,0] having 8 values.
        # A similar thing will happen if '8' and '9' are missing.

        # The user can override the above settings by specifying the width of the
        # binary vector desired.

        if num_classes == -1:
            # Detect width of binary vectors from dataset.
            num_classes = np.max(detected_classes) - \
                np.min(detected_classes) + 1

        # The dataset of binary vectors.
        out_dataset = np.zeros((len(dataset_targets), num_classes))
        min_detected_class = np.min(detected_classes)
        for i in range(0, len(dataset_targets)):  # For each example:
            out_dataset[i][dataset_targets[i] - min_detected_class] = 1
        return out_dataset


class Error:
    error_function = ""

    def __init__(self, error_function):
        self.error_function = error_function

    def calculate_error(self, predicted_outputs, actual_outputs):
        '''
        :param predicted_outputs: a (num_examples)*(num_outputs) NumPy matrix.
        :param actual_outputs: a (num_examples)*(num_outputs) NumPy matrix.
        :return: scalar error value (unregularized).
        '''
        if self.error_function.lower() == "log":
            return self._log_error(predicted_outputs, actual_outputs)
        elif self.error_function.lower() == "mean_squared" or self.error_function.lower() == "mean squared":
            return self._mean_squared_error(predicted_outputs, actual_outputs)

    def _log_error(self, predicted_outputs, actual_outputs):
        m = len(predicted_outputs)
        y = actual_outputs
        h_theta = predicted_outputs
        error = (-1 / m) * np.sum(y * np.log(h_theta) + (1 - y) * h_theta)
        return error

    def _mean_squared_error(self, predicted_outputs, actual_outputs):
        M = len(predicted_outputs)
        # Calculates a scalar value.
        error = (1 / (2 * M)) * np.sum((actual_outputs - predicted_outputs)**2)
        return error


class ErrorDerivative:
    '''Calculates the derivative of the error function at the OUTPUT layer of the network, which "seeds" the backpropagation or error gradients.
    This value is an (M_b, K) matrix. We elementwise-multiply it with the output layer's local gradients, to get the
    Error gradient at the output layer.'''
    error_function = ""

    def __init__(self, error_function):
        self.error_function = error_function

    def calculate_error_derivative(self, predicted_outputs, actual_outputs):
        '''
        :param predicted_outputs: An (M_b, K) matrix of (encoded) outputs predicted by the output layer of the network
                when we input a (M_b, F) batch of traning examples.
        :param actual_outputs: An (M_b, K) matrix of actual outputs which we get from the dataset.
        :return:
        '''
        if self.error_function.lower() == "mean_squared" or self.error_function.lower() == "mean squared":
            return self._mean_squared_error_derivative(predicted_outputs, actual_outputs)

    def _mean_squared_error_derivative(self, predicted_outputs, actual_outputs):
        # Error = E = MSE = (1/2).((o-y)^2)  (<-half of squared difference)
        # d/do(MSE) = (o - y)          (in general, an (M_b, K) matrix).
        # where o = predicted outputs  (in general, an (M_b, K) matrix),
        # and   y = actual outputs     (in general, an (M_b, K) matrix).
        return predicted_outputs - actual_outputs


class WeightUpdate:
    weight_update_rule = ""
    weight_update_params = {}

    def __init__(self, weight_update_rule, weight_update_params):
        self.weight_update_rule = weight_update_rule
        self.weight_update_params = weight_update_params

    def calculate_update(self, current_layer_inputs, current_layer_gradient):
        if self.weight_update_rule.lower() == "standard" or self.weight_update_rule.lower() == "gradient descent":
            return self._standard_weight_update(current_layer_inputs, current_layer_gradient)

    def _standard_weight_update(self, current_layer_inputs, current_layer_gradient):
        # alpha = learning rate = the step size to take in gradient descent.
        alpha = self.weight_update_params.get('alpha')
        if alpha == None:
            self.weight_update_params.get('learning_rate')
            if alpha == None:
                raise Exception("For the standard gradient descent weight update rule, a value of `alpha` must be specified in the dictionary `weight_update_params`")
        out_mat = np.dot(current_layer_inputs.T, current_layer_gradient)
        out_mat *= (-1*alpha)	## This is performed in place. Source: https://stackoverflow.com/a/4319312/4900327
        gc.collect()
        return out_mat


class Regularization:
    regularization_type = ""
    regularization_params = {}

    def __init__(self, regularization_type, regularization_params):
        self.regularization_type = regularization_type
        self.regularization_params = regularization_params

    def regularize(self, num_examples, weights):
        '''
        :param num_examples: The number of examples in the dataset/batch that we must regularize over.
        :param weights: A list of Numpy Matrices, representing weights.
        :return: scalar regularization value.
        '''
        if self.regularization_type.lower() == 'l2' or self.regularization_type.lower() == 'ridge':
            return self._L2_regularization(num_examples, weights, self.regularization_params.get('lambda_'))
        if self.regularization_type.lower() == 'l1' or self.regularization_type.lower() == 'lasso':
            return self._L1_regularization(num_examples, weights, self.regularization_params.get('lambda_'))

    def _L2_regularization(self, num_examples, weights, lambda_):
        # If it's neither an int or a float:
        if type(lambda_) != type(1) and type(lambda_) != type(1.5):
            raise ValueError(
                "In L2 (Ridge) Regularization, parameter `lambda_` must be an integer of a float (currently, it is `%s`)" % (lambda_))

        # L2 Regularization (Ridge):
        m = num_examples
        regularization_value = 0
        for w in weights:
            regularization_value += np.sum(w ** 2)  # (w)^2
        regularization_value = (lambda_ / (2 * m)) * regularization_value
        return regularization_value

    def _L1_regularization(self, num_examples, weights, lambda_):
        # If it's neither an int or a float:
        if type(lambda_) != type(1) and type(lambda_) != type(1.5):
            raise ValueError(
                "In L1 (Lasso) Regularization, parameter `lambda_` must be an integer of a float (currently, it is `%s`)" % (lambda_))

        # L1 Regularization (Lasso):
        m = num_examples
        regularization_value = 0
        for w in weights:
            regularization_value += np.sum(np.abs(w))  # abs(w)
        regularization_value = (lambda_ / (2 * m)) * regularization_value
        return regularization_value


class Network:
    ''' Represent a network as a list of NumPy matrices.'''
    # Note that all the following values are invalid and only show the type of
    # data we want for that parameter:

    weights = {}    ## The dict of matrices to be trained, and later used for prediction.
    '''
    We consider each matrix as corresponding to a layer here. We imagine that the input layer is nascent and
      doesn't really exist.
    All layers are hidden layers, except the last one which is "the output layer".
      So when 'third layer' is mentioned, it means the third hidden layer (unless there are only three
      matrices/layers, in which case it is the output layer).

    In each weight matrix, each column is the weight vector of a particular neuron.
    The number of rows is the number of inputs it takes from the neuron before it.
    There will be 1 extra row at the bottom of each matrix to account for the bias unit.

    EACH NEURON TRAINS WEIGHTS FOR THE INPUTS IT RECIEVES, NOT THE OUTPUTS IT DELIVERS. This is crucial.
    There's also only one output value, it's just passed on to all the neurons in the next layer (except the bias).

    The bias units are NOT neurons; they are nascent like the input layer and have no weights attached to them.

    So for example if we have a network which has N inputs and K outputs, and three hidden layers
      having [J1, J2, J3] nodes each, then we have 4 layers, each of which corresponds to a matrix.
      Going from left to right:
        - The input layer is vapour/nascent, and has no attached weights or activation functions.
            It only supplies a stream of data.
        - The first hidden layer is represented by a (N+1)x(J1) matrix. The +1 is because of the bias weights used to augment each neuron; this gets added a the last row, since each column in a neuron.
            In the first layer, each neuron takes as input (N+1) values: N from the dataset and one bias unit.
              It trains weights for each of these, since A NEURON'S WEIGHTS ARE TRAINED FOR ITS INPUTS.
            Thus, we will have (J1) neurons in the first layer.
        - Similar to the logic above, the 2nd hidden layer is represented by a (J1+1)x(J2) matrix, and has (J2) neurons.
        - Similar to the logic above, the 3rd hidden layer is represented by a (J2+1)x(J3) matrix, and has (J3) neurons.
        - Similar to the logic above, the   OUTPUT layer   is represented by a (J3+1)x(K)  matrix, and has (K) neurons, each of
            which outputs one output value.
    '''

    num_features = -1
    num_outputs = -1
    num_nodes_in_hidden_layers = []
    bias = 0

    weight_initialization_obj = None
    activation_obj = None
    local_gradient_obj = None
    encoding_obj = None
    error_obj = None
    error_derivative_obj = None
    weight_update_obj = None
    regularization = False
    regularization_obj = None

    dataset_normalizations = None

    printing = False
    out_log = None
    properties_text = ""

    def __init__(self, num_features, num_outputs, num_nodes_in_hidden_layers=[],
                 weight_initialization="standard",
                 weight_init_params={'negative_weights': True, 'div_by': 1.0},
                 problem_type="classification",
                 problem_params={'local_representation': True},
                 activation_function="sigmoid",
                 error_function="mean_squared",
                 weight_update_rule="standard", weight_update_params={'alpha': 0.1},
                 regularization=True, regularization_type="L1", regularization_params={'lambda_': 1},
                 bias=1,
                 printing=False):

        self.weight_initialization_obj = WeightInitialization(
            weight_initialization, weight_init_params)
        self.weights = self.weight_initialization_obj.initialize(
            num_features, num_outputs, num_nodes_in_hidden_layers)

        # Number of neurons in each layer:
        self.num_features = num_features
        self.num_outputs = num_outputs
        self.num_nodes_in_hidden_layers = num_nodes_in_hidden_layers

        # Define the problem type (e.g. classification, regression, etc.)
        self.encoding_obj = Encoding(problem_type, problem_params)

        # Bias value:
        self.bias = bias

        # Activation function to use:
        self.activation_obj = Activation(activation_function)

        # Gradient functions to use:
        self.local_gradient_obj = LocalGradient(activation_function)

        # Error function to use:
        self.error_obj = Error(error_function)
        self.error_derivative_obj = ErrorDerivative(error_function)

        # Regularization function to use:
        self.regularization = regularization
        if self.regularization:
            self.regularization_obj = Regularization(
                regularization_type, regularization_params)

        # Update rule to use:
        self.weight_update_obj = WeightUpdate(
            weight_update_rule, weight_update_params)

        # Whether we print the intermediate results or not.
        self.printing = printing

        self.out_log = io.open('nn_most_recent.log', 'w')
        self.out_log.write('Current process pid: %s \n'%(os.getpid()))

        nn_props_text = '\n'
        nn_props_text += '>' * 50 +'\n'
        nn_props_text += "%30s"%"Network settings:" +'\n'
        nn_props_text += '<' * 50 +'\n'
        nn_props_text += "\n  - Problem type:          %15s    params: %s"%(problem_type, problem_params) +'\n'
        nn_props_text += "\n  - Network architecture (number of neurons in each layer):" +'\n'
        nn_props_text += "\tInput layer (including bias of %s):            %5s"%(bias, num_features+1) +'\n'
        for i in range(0, len(num_nodes_in_hidden_layers)):
            nn_props_text += "\tHidden Layer no. %3s (including bias of %2s):  %5s"%(i+1, bias, num_nodes_in_hidden_layers[i]+1) +'\n'
        nn_props_text += "\tOutput layer (in binary-vectors):             %5s"%(num_outputs) +'\n'
        nn_props_text += '\n'
        nn_props_text += "\tMatrix data type: %s (%s bytes)"%(self.weights[1].dtype, self.weights[1].itemsize) +'\n'
        nn_props_text += "\tTotal Neural Network size in RAM: %5.3f MB"%( sum([self.weights[k].nbytes/1e6 for k in self.weights]) ) +'\n'
        nn_props_text += "\n  - Weight initialization:  %15s    params: %s"%(weight_initialization, weight_init_params) +'\n'
        nn_props_text += "\n  - Activation function:    %15s"%(activation_function) +'\n'
        nn_props_text += "\n  - Error function:         %15s"%(error_function) +'\n'
        nn_props_text += "\n  - Weight update rule:     %15s    params: %s"%(weight_update_rule, weight_update_params) +'\n'
        if regularization:
            nn_props_text += "\n  - Regularization type:    %15s    params: %s"%(regularization_type, regularization_params) +'\n'
        nn_props_text += "\n"+('-'*50)+"\n"

        self.properties_text = nn_props_text
        self.out_log.write(self.properties_text)
        self.out_log.flush()
        print(self.properties_text)



    def _add_bias(self, current_layer_outputs):
        # Add a bias column, to denote the bias neuron/unit. Remember that each
        # column is a neuron.
        # Makes a (num_examples)x1 matrix, initialized to self.bias.
        bias_col = np.full((current_layer_outputs.shape[0], 1), self.bias)
        # Source: http://stackoverflow.com/a/20688968/4900327
        return np.append(current_layer_outputs, bias_col, axis=1)

    def _remove_final_row(self, next_layer_weight_mat):
        # Remove the final row of a matrix
        # During backprop, the error gradient for a particular layer l_c is calculated using the error gradient and
        # the weights of  the next layer, l_(c+1). Since the bias unit of the next layer does not pass any value backwards
        # to the current layer l_c. Thus, though the weight matrix of the next layer l_(c+1) has dimensions
        # ( N_c + 1,  N_(c+1) ), we don't need to pass back the weights of the bias unit. Thus, we lop off the last row,
        # making it ( N_c + 1,  N_(c+1) ).
        # Note that we don't modify the actual matrix, we only ptovide a view
        # to it.
        return next_layer_weight_mat[: len(next_layer_weight_mat) - 1]

    def _random_shuffle_dataset(self, dataset_inputs, dataset_targets):
        p = np.random.permutation(len(dataset_inputs))
        return dataset_inputs[p], dataset_targets[p]

    def _dataset_normalize(self, dataset_inputs):
        # Use min-max normalization with zero-centering:
        # The +1 is added to prevent numpy.nan when we do 0/0.
        if self.dataset_normalizations == None:
            self.dataset_normalizations = {}
            self.dataset_normalizations['avg'] = np.average(dataset_inputs, axis=0)
            self.dataset_normalizations['max'] = np.max(dataset_inputs, axis=0)
            self.dataset_normalizations['min'] = np.min(dataset_inputs, axis=0)

        # Source: stackoverflow.com/a/29661707/4900327
        dataset_inputs -= self.dataset_normalizations['avg']
        array_to_div_by = self.dataset_normalizations['max'] - self.dataset_normalizations['min'] + 1
        dataset_inputs /= array_to_div_by
        return dataset_inputs

    def _dimensionality_reduction(self, dataset_inputs, new_num_features):
        from sklearn.decomposition import PCA
        pca = PCA(new_num_features)
        pca.fit(dataset_inputs)
        return np_arr(pca.transform(dataset_inputs))

    def _training_accuracy(self, dataset_inputs, actual_outputs, accuracy_sample_size):
        '''
        :param predicted_outputs:   An (M_b, K) matrix of binary (0 and 1) values, where only one value per row is 1.
        :param actual_outputs: An (M_b, K) matrix of binary (0 and 1) values, where only one value per row is 1.
        :return:
        '''

        if type(accuracy_sample_size) == type(1) and accuracy_sample_size > 0:
            M = len(dataset_inputs)
            r = random.randrange(0, M - accuracy_sample_size)
            predicted_outputs = self.forward_pass(
                dataset_inputs[r: r + accuracy_sample_size])

            num_misclassifications = np.sum(
                np.abs(predicted_outputs - actual_outputs[r: r + accuracy_sample_size]) / 2)
            acc = 100 * (accuracy_sample_size -
                         num_misclassifications) / accuracy_sample_size

        else:  # Use the entire dataset.
            M = len(dataset_inputs)
            predicted_outputs = self.forward_pass(dataset_inputs)
            num_misclassifications = np.sum(
                np.abs(predicted_outputs - actual_outputs) / 2)
            acc = 100 * (M - num_misclassifications) / M
        return acc

    def forward_pass(self, batch, backpropagation=False):
        printing = self.printing
        if printing:
            print("\n\n")
            print('<' * 50)
            if backpropagation:
                print("STARTING FORWARD PASS FOR BACKPROPAGATION:")
            else:
                print("STARTING FORWARD PASS FOR OUTPUT PREDICTION:")
            print('>' * 50)
            print("\n\n")

        if len(batch.shape) == 1:  # For the case of batch = input vector `x`
            batch = batch.reshape(batch.shape[0], 1).T

        if len(batch.shape) != 2 or (batch.shape[1] != self.num_features):
            return None

        # Now, batch should have shape: (Batch _size, num_features) i.e. (M_b,
        # F)
        if printing:
            print("\n")
            print('-' * 50)
            print("Batch from dataset: (%s,%s)\n" %
                  (batch.shape[0], batch.shape[1]))
            print(batch)

        # We add the bias terms as the final column for each row. Remember that each column is a neuron.
        # Thus here we have a bias neuron on the input layer, feeding into each neuron of the first hidden layer.
        # Thus, we make current_layer_inputs with dimensions (M_b,  F + 1).
        current_layer_inputs = self._add_bias(batch)

        if backpropagation:
            # We must store the inputs to each layer for use in backpropagation. These are the outputs
            # of the previous layers. We assume the first hidden layer takes inputs from the pseudo-input layer's "outputs",
            # which are the traning examples from the dataset batch.
            inputs = {}

            # We must store the local gradients (the (M_b, N_c) matrix of F'(net) values) for each hidden layer,
            # for use in backpropagation:
            local_gradients = {}

        current_layer_outputs = None

        # The current HIDDEN layer number. We use this to index `inputs` and
        # `local_gradients`.
        for current_layer_num in sorted(self.weights.keys()):
            if printing:
                print("\n")
                print('-' * 50)
                print("Performing calculations for Hidden Layer H%s:" %
                      current_layer_num)
                print('-' * 50)

            # Starting from current_layer = 1st hidden layer, use the weights to calculate the outputs of the current layer.
            # If we are performing backpropagtion, also store the inputs to the
            # current layer and the local gradients.
            # An (M_b, N_c) matrix of "net" values.
            current_layer_excitations = np.dot(
                current_layer_inputs, self.weights[current_layer_num])
            gc.collect()

            if backpropagation:
                # Store the inputs for (i+1)th hidden layer:
                inputs[current_layer_num] = current_layer_inputs
                gc.collect()

                # Calculate gradient values for (i+1)th hidden layer:
                local_gradients[current_layer_num] = self.local_gradient_obj.calculate_local_gradient(current_layer_excitations)
                gc.collect()

            # Calculate the outputs from the current layer.
            # An (M_b, N_c) matrix.
            current_layer_outputs = self.activation_obj.activate(current_layer_excitations)
            gc.collect()

            if printing:
                print("\n")
                print('-' * 50)
                print("Biased inputs to hidden layer H%s: (%s,%s)\n" % (current_layer_num, current_layer_inputs.shape[0], current_layer_inputs.shape[1]))
                print(current_layer_inputs)

                print("\n")
                print('-' * 50)
                print("Weights for hidden layer H%s: (%s,%s)\n" % (
                    current_layer_num, self.weights[current_layer_num].shape[0], self.weights[current_layer_num].shape[1]))
                print(self.weights[current_layer_num])

                if backpropagation:
                    print("\n")
                    print('-' * 50)
                    print("Local gradients for hidden layer H%s: (%s,%s)\n" % (
                        current_layer_num, local_gradients[current_layer_num].shape[0], local_gradients[current_layer_num].shape[1]))
                    print(local_gradients[current_layer_num])

                print("\n")
                print('-' * 50)
                print("Output from hidden layer H%s: (%s,%s)\n" % (
                    current_layer_num, current_layer_outputs.shape[0], current_layer_outputs.shape[1]))
                print(current_layer_outputs)

            # The inputs for the next layer are the outputs of the previous
            # layer, with the bias term.
            # An (M_b,  N_c + 1) matrix.
            current_layer_inputs = self._add_bias(current_layer_outputs)

            # Free some RAM:
            del current_layer_excitations
            gc.collect()

        # The biased inputs to the next layer, used in the loop to pass the
        # outputs to the
        del current_layer_inputs
        gc.collect()  # next iteration of the loop. Now, it represents the biased outputs of the output layer,
        # a meaningless qauntity since the final layer's outputs are not biased.
        # `current_layer_outputs` is the actual, unbiased outputs of the final layer.
        # Hence, we safely delete `current_layer_inputs` and free RAM space.

        # Checking if `current_layer_outputs` is valid:
        if current_layer_outputs == None:
            raise Exception(
                "The forward pass yeilded no output, something is probably wrong with the batch or the weights.")

        # If it is valid, we've compleed the forward pass:
        # Presently, these are outputs of the activation function of the final
        # layer.
        final_outputs = current_layer_outputs
        # We must now encode these final_outputs according to our problem:
        # - For classification, we convert each sigmoid (or whatever) output into a binary (0 and 1) one.
        # We do so by setting the max output value as 1 for each training example, and setting all the others to zero.
        # - For regression, we leave it alone (for now) since I have no idea how to deal with it.

        # Print the final outputs, without the encoding:
        if printing:
            print("\n")
            print('-' * 50)
            print("Encoding outputs:")
            print('-' * 50)

            print("\n")
            print('-' * 50)
            print("Unencoded outputs: (%s,%s)\n" %
                  (final_outputs.shape[0], final_outputs.shape[1]))
            print(final_outputs)

        # Encode the outputs:
        final_outputs = self.encoding_obj.encode_final_output(final_outputs)

        # Print the final outputs, with the encoding:
        if printing:
            print("\n")
            print('-' * 50)
            print("Final, encoded outputs outputs: (%s,%s)\n" %
                  (final_outputs.shape[0], final_outputs.shape[1]))
            print(final_outputs)

            print("\n")
            print('<' * 50)
            print("DONE WITH FORWARD PASS")
            print('>' * 50)
            print("\n\n")

        # Return the final outputs:
        if backpropagation == True:
            return final_outputs, inputs, local_gradients

        return final_outputs

    def backward_pass(self, batch, batch_targets):
        '''
        :param batch: An (M_b, F) matrix of training example input values. F = number of features.
        :param batch_targets: An (M_b, K) matrix of training example targets/teacher values. K = number of target classes.
        :return: The error value. The `self.weights` matrices must be updated in this function.
        '''
        weight_updates = {}

        final_outputs, inputs, local_gradients = self.forward_pass(
            batch, backpropagation=True)

        # Calculate the error in the predictions of the entire batch:
        error_val = self.error_obj.calculate_error(
            final_outputs, batch_targets)

        # The layer number of the output layer
        output_layer_num = sorted(local_gradients.keys())[-1]

        # Now, we calculate the error gradients separately for the output
        # layer, since it's different from the other layers.
        output_layer_error_derivatives = self.error_derivative_obj.calculate_error_derivative(
            predicted_outputs=final_outputs, actual_outputs=batch_targets)

        # `output_layer_error_gradients`, or DELTA_l_o, is the elementwise multiplication of:
        # - E'(o, y) => the output_layer_error_derivatives, a (M_b, K) matrix.
        # - D => the local_gradients, calculated during forward pass, a (M_b, K) matrix.
        # The result is a (M_b, K) matrix.

        output_layer_error_gradients = output_layer_error_derivatives * \
            local_gradients[output_layer_num]

        # Calulate weight update for the output layer:
        weight_updates[output_layer_num] = self.weight_update_obj.calculate_update(inputs[output_layer_num],
                                                                                   output_layer_error_gradients)
        # ( M_b,  N_(c+1) ) matrix.
        next_layer_error_gradients = output_layer_error_gradients

        current_layer_gradient = None

        ## Calculate weight updates and gradients for hidden layers,
        ## from the penultimate layer backwards upto the first hidden layer:
        for current_layer_num in range(output_layer_num-1, 0, -1):
            next_layer_weights_without_next_layer_bias = self._remove_final_row( self.weights[current_layer_num+1] )
            B = np.dot(next_layer_error_gradients, next_layer_weights_without_next_layer_bias.T)
            gc.collect()
            current_layer_gradient = B * local_gradients[current_layer_num] ## Element-wise multiplication.
            del B
            gc.collect()

            weight_updates[current_layer_num] = self.weight_update_obj.calculate_update(inputs[current_layer_num],
                                                                                        current_layer_gradient)
            next_layer_error_gradients = current_layer_gradient

            # Free some RAM
            gc.collect()

        # We are done with the calculation.
        # Free some RAM:
        del next_layer_error_gradients
        del current_layer_gradient
        gc.collect()

        # Update each weight matrix:
        for i in sorted(self.weights.keys()):
            max_update = np.max(weight_updates[i])
            min_update = np.min(weight_updates[i])
            # print("max_update: %s    min_update: %s"%(max_update, min_update))
            self.weights[i] += weight_updates[i]
            # print("Weight updates:")
            # print(weight_updates[i])
        # print("<<<<< Updated weights >>>>>\n")

        return error_val

    def train(self, dataset_inputs, dataset_targets, batch_size, num_recycles=1, shuffle_dataset=True, normalize_dataset=True, reduced_num_dimensions=None, plot_errors_and_accuracies=False, accuracy_metric="training_set", accuracy_sample_size=None):
        '''
        :param dataset_inputs: An (M, F) matrix of inputs for the training set.
        :param dataset_targets: An (M, K) matrix of targets for the training set.
        :param batch_size: The size of the batch
        :param num_recycles: Number of times to
        :param shuffle_dataset: True or False
        :param normalize_dataset: True or False
        :param plot_errors_and_accuracies: True or False
        :param accuracy_metric: Either 'batch' or 'training_set', to denote which to use for training accuracy.

        :return: error_values, accuracy_values : a list of each, each element is the value after tranining on one batch.
        '''

        if normalize_dataset:
            dataset_inputs = self._dataset_normalize(dataset_inputs)
            print("Normalized dataset.")
            gc.collect()

        if type(reduced_num_dimensions) == type(1):
            # NOTE: you MUST normalize before PCA.
            dataset_inputs = self._dimensionality_reduction(
                dataset_inputs, reduced_num_dimensions)
            print("Reduced each training example to %s dimensions" %
                  reduced_num_dimensions)

        M = len(dataset_inputs)
        if len(dataset_targets) != M:
            raise Exception(
                "For training, dataset inputs and targets must be the same number of rows.")

        ## We're done messing with the datset.
        print("Dataset stats: datatype = %s; size = %5.3f MB."%(dataset_inputs.dtype, dataset_inputs.nbytes/1e6))

        if plot_errors_and_accuracies:
            window = plt.figure()

            # 1x2 matrix of subplots, select first one (left one)
            error_plot = window.add_subplot(1, 2, 1)
            error_plot.set_xlabel("Number of batches trained")
            error_plot.set_ylabel("Error")

            # 1x2 matrix of subplots, select second one (right one)
            acc_plot = window.add_subplot(1, 2, 2)
            acc_plot.set_xlabel("Number of batches trained")
            acc_plot.set_ylabel("Training accuracy")

            plt.show()

            last_plotted_error_index = 0
            last_plotted_acc_index = 0

            colors = ['b', 'g', 'r', 'y', 'k']
            error_plot_color = colors[random.randrange(0, len(colors))]
            acc_plot_color = colors[random.randrange(0, len(colors))]

            training_start_time = time.time()

        error_values = []
        accuracy_values = []
        num_batches_trained = 0
        best_acc = -1

        for j in range(0, num_recycles):
            if shuffle_dataset:
                dataset_inputs, dataset_targets = self._random_shuffle_dataset(
                    dataset_inputs, dataset_targets)

            # Perform one pass over the dataset, training in batches:
            for i in range(0, M, batch_size):
                error = self.backward_pass(
                    dataset_inputs[i: i + batch_size], dataset_targets[i: i + batch_size])
                error_values.append(error)

                if accuracy_metric.lower() == 'batch':
                    acc = self._training_accuracy(dataset_inputs[
                                                  i: i + batch_size], dataset_targets[i: i + batch_size], accuracy_sample_size=None)
                elif accuracy_metric.lower() == 'training_set':
                    acc = self._training_accuracy(
                        dataset_inputs, dataset_targets, accuracy_sample_size)
                accuracy_values.append(acc)

                ## Write to file if the model is the best seen in this run.
                current_time = datetime.now()
                if acc > best_acc:
                    ## Using klepto to save a 'directory archive'. Source: https://stackoverflow.com/a/31627466/4900327
                    out_dir = klepto.archives.dir_archive('best_nn_model', cached=True, serialized=True)
                    for key in self.weights:
                        out_dir[key] = self.weights[key]
                        gc.collect()
                        out_dir.dump()
                        out_dir.clear()
                        gc.collect()

                    with io.open('best_nn_stats.txt', 'w') as out:
                        out.write(' accuracy: %s \n error: %s \n batch no: %s \n timestamp: %s \n'%(acc, error, len(accuracy_values), current_time))
                    best_acc = acc

                log_text = "Batch no. %s; \t Current accuracy: %3.3f%% \t Current error: %3.8f; \t Timestamp: %s"%(len(accuracy_values), acc, error, current_time)
                self.out_log.write(log_text+'\n')
                self.out_log.flush()
                print(log_text)

                ## Free some RAM:
                gc.collect()

                # Plot the errors and accuracies:
                if plot_errors_and_accuracies:
                    e = len(error_values)
                    # Number of batches whose errors to plot at once.
                    if e > 1 and e - last_plotted_error_index >= 1:
                        plot_y = []
                        plot_x = []
                        for p in range(last_plotted_error_index, e):
                            plot_x.append(p)
                            plot_y.append(error_values[p])
                        error_plot.plot(plot_x, plot_y, color=error_plot_color)
                        plt.pause(0.0001)
                        last_plotted_error_index = e - 1

                    t = len(accuracy_values)
                    # Number of batches whose accuracies to plot at once.
                    if t > 1 and t - last_plotted_acc_index >= 1:
                        plot_y = []
                        plot_x = []
                        for p in range(last_plotted_acc_index, t):
                            plot_x.append(p)
                            plot_y.append(accuracy_values[p])
                        acc_plot.plot(plot_x, plot_y, color=acc_plot_color)
                        plt.pause(0.0001)
                        last_plotted_acc_index = e - 1

                    window.suptitle("Running for %.3f seconds" %
                                    (time.time() - training_start_time))

                num_batches_trained += 1
                time.sleep(0.5)
                if num_batches_trained % 100 == 0:
                    time.sleep(60)  # Useful for when leaving overnight.
            # print("Completed one cycle")

        # Free some RAM:
        gc.collect()
        self.out_log.close()
        return error_values, accuracy_values


# Testing out forward pass:

def test_forward_pass(num_examples, backpropagation):
    if num_examples > 1e6:
        print("this number of examples messed up my ram, don't use")
        return
    num_examples = int(num_examples)
    num_features = 4
    num_outputs = 3
    start = time.time()
    dataset = np.round((np.random.rand(num_examples, num_features) -
                        np.random.rand(num_examples, num_features)) * 1000)
    end = time.time()
    print("\nGenerated dataset with %s examples in %3s seconds" %
          (num_examples, end - start))

    nn = Network(num_features, num_outputs, [10, 10], printing=True)
    print("\nCreated network object.")

    start = time.time()

    # Calculate the forward pass:
    output = nn.forward_pass(dataset, backpropagation=backpropagation)

    end = time.time()

    if backpropagation:
        print("\nCompleted forward pass (with backpropagation calculation) in %3s seconds: \n\tDataset: %s examples, each with %s features. \n\tNetwork: hidden layers have %s neurons" % (
            end - start, num_examples, num_features, nn.num_nodes_in_hidden_layers + [nn.num_outputs]))
    else:
        print("\nCompleted forward pass (without backpropagation calculation) in %3s seconds: \n\tDataset: %s examples, each with %s features. \n\tNetwork: hidden layers have %s neurons" % (
            end - start, num_examples, num_features, nn.num_nodes_in_hidden_layers + [nn.num_outputs]))

# test_forward_pass(20)
# test_forward_pass(20, backpropagation=True)

import iris_dataset


def test_training_iris():
    train_inputs, train_targets = iris_dataset.load_iris_training_set(
        shuffle=True)
    num_features = len(train_inputs[0])
    num_outputs = len(train_targets[0])
    nn = Network(num_features, num_outputs, [100, 100, 100], activation_function="leaky relu", weight_update_params={'alpha': 0.01}, printing=False)
    error_vals, acc_vals = nn.train(train_inputs, train_targets, batch_size=20, num_recycles=100, shuffle_dataset=True,
                                    plot_errors_and_accuracies=False, accuracy_metric="training_set", accuracy_sample_size=80)

    input("(Press ENTER to exit)\n> ")
# test_training_iris()


from MNIST_dataset_for_Digit_Recognition import MNIST_handler
sys.path.append('./MNIST_dataset_for_Digit_Recognition')

def test_training_MNIST(num_examples=5000):
    dataset = {}
    train_inputs, train_targets, test_inputs, test_targets = MNIST_handler.NUMPY.load_all()
    train_inputs = train_inputs.astype('float64', copy=False)
    test_inputs = test_inputs.astype('float64', copy=False)
    gc.collect()

    train_targets = Encoding.convert_outputs_integers_to_binary_vectors(train_targets)
    print("Converted targets to binary vectors")

    r = 0
    if num_examples < len(train_inputs):
        r = random.randrange(0, len(train_inputs) - num_examples)

    train_inputs = train_inputs[r: r + num_examples]
    train_targets = train_targets[r: r + num_examples]

    num_features = len(train_inputs[0])
    num_outputs = len(train_targets[0])
    nn = Network(num_features, num_outputs, [5000, 5000, 5000],
                    weight_initialization = 'standard', weight_init_params = {'negative_weights':True, 'div_by':3.0},
                    activation_function="leaky relu", weight_update_params={'alpha': 0.00003}, printing=False)
    error_vals, acc_vals = nn.train(train_inputs, train_targets, batch_size=30, num_recycles=10, shuffle_dataset=True, reduced_num_dimensions=None, plot_errors_and_accuracies=False, accuracy_metric="training_set", accuracy_sample_size = 1000)
    input("(Press ENTER to exit)\n> ")

test_training_MNIST(60000)
