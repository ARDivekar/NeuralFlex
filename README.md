NeuralFlex
-------------
An extensible Neural Network library written from scratch using Python's NumPy. 
Also includes convenience functions to extract the MNIST dataset in various formats (CSV, JSON, Python LISTS, NUMPY arrays).

Author: [Abhishek Divekar](https://www.linkedin.com/in/abhishek-divekar-01bb71bb).

Dependencies:
-------------

- Python3
- NumPy: matrix multiplication and processing.
- Klepto: save the network to disk and restore it at a later point.

Usage:
-------------

 1. `Network.py` contains all the code
 2. Performance has been tested on two datasets at present: 
	2.1 Fisher's [IRIS](https://www.wikiwand.com/en/Iris_flower_data_set), a tiny dataset of species of the Iris flower. It is found in its entirety in `iris_dataset.py`
	 2.2 [MNIST](http://yann.lecun.com/exdb/mnist/), a standard Digit Recognition dataset commonly used to train OCR models.
	 - Each sample is a 28x28 monochrome image, with each pixel having a greyscale values from 0 to 255.
	 - 60,000 training and 10,000 test images.
	 - To work with MNIST dataset (with or without this library), open the directory `MNIST_dataset_for_Digit_Recognition/` and follow its `INSTRUCTIONS.txt` to get the dataset in various formats. 
	 To use `Network.py` with MNIST, you must create the `NUMPY` format.


The `References/` folder has some (very readable) documents to better understand the Backpropagation algorithm. 
I'm writing my own in `Backpropagation writeup/`, with an explicit and easy-to-understand notation aimed at programmers.
